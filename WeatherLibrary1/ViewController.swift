//
//  ViewController.swift
//  WeatherLibrary1
//
//  Created by WSr on 07/10/2019.
//  Copyright © 2019 WSr. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    @IBOutlet weak var CityText: UITextField!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var myButton: UIButton!
    
    var city = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if city != "" {
                  let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&apiKey=6da8e8bdb22e1d408ffb437eab399b45"
              let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
              
                  // залрос через библиотеку
                  Alamofire.request(url!, method: .get).validate().responseJSON { response in
                     switch response.result {
                     case .success(let value):
                         let json = JSON(value)
                         print(json["main"]["temp"].stringValue) // вывод температуры
                         self.cityLabel.text = self.CityText.text! + " " + json["main"]["temp"].stringValue
                     case .failure(let error):
                         print(error)
                     }
                 }
              }
        // закругление Buttun
        myButton.layer.borderWidth = 1
        myButton.layer.cornerRadius = 10
        
    }

    @IBAction func action(_ sender: Any) {
        
        if city != "" {
            let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&apiKey=6da8e8bdb22e1d408ffb437eab399b45"
        let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
            // залрос через библиотеку
            Alamofire.request(url!, method: .get).validate().responseJSON { response in
               switch response.result {
               case .success(let value):
                   let json = JSON(value)
                   print(json["main"]["temp"].stringValue) // вывод температуры
                   self.cityLabel.text = self.CityText.text! + " " + json["main"]["temp"].stringValue
               case .failure(let error):
                   print(error)
               }
           }
        }
    }
   
}

