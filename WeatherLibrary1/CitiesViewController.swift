//
//  CitiesViewController.swift
//  WeatherLibrary1
//
//  Created by WSr on 07/10/2019.
//  Copyright © 2019 WSr. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
 // ссылка на таблицу
    @IBOutlet weak var cityTable: UITableView!
    //
    var cityArray = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //подписываемся на делегат (действия в таблице) и dataSource заполнение таблицы
        cityTable.delegate = self
        cityTable.dataSource = self
    }
    // количество ячеек
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //количество элементов в массиве
        return cityArray.count
     }
     // отрисовка ячейки
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //создаем переменную с ячейкой/ важно указать свой идентификатор ячейки из сториборда
        let cell = cityTable.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
        // присваиваем текст стандартному Label из массива по индексу
        cell.textLabel?.text = cityArray[indexPath.row]
        return cell
     }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let selectCell = cityTable.indexPathForSelectedRow
        let vc = segue.destination as! ViewController
        vc.city = cityArray[selectCell!.row]
    }
    
    
    @IBAction func addnewCity(_ sender: Any) {
        //создание
        let alert = UIAlertController(title: "Добавление города", message: "Введите ваш город", preferredStyle: .alert)
        alert.addTextField { UITextField in UITextField.placeholder = "City"
        }
        let confirmAction = UIAlertAction(title: "Ok", style: .default) { [weak alert] _ in
            guard let textField = alert?.textFields?.first else { return }
            //добавление в массив города
            self.cityArray.append(textField.text!)
            //обновление таблицы
            self.cityTable.reloadData()
        
        }
        // создание действия отмена
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        //добавнление на экран
        present(alert, animated: true, completion: nil)
        // удаление ячеек
        func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {
                self.cityArray.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                self.cityTable.reloadData()
            }
        }
        
    }
    
}
